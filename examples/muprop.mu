Mu Property:

!(
    (nu X . (!csend &
    [@def__varname]ff
    &
    [\use__varname usedef__varname]X))
    &
    ~[\def__varname usedef__varname](nu
    Y.(mu Z. csend |
    <@def__varname>tt | 
    <\use__varname usedef__varname>Z) & 
    ~[\def__varname usedef__varname]Y)
)
