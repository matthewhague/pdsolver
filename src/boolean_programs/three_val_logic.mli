

type three_val = T | F | N


val or3 : three_val -> three_val -> three_val
val and3 : three_val -> three_val -> three_val
val xor3 : three_val -> three_val -> three_val
val eq3 : three_val -> three_val -> three_val
val neq3 : three_val -> three_val -> three_val
val imp3 : three_val -> three_val -> three_val

val ite3 : three_val -> three_val -> three_val -> three_val 

val not3 : three_val -> three_val
