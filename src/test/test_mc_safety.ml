open Pds;;
open Parity_game;;
open Words;;
open State;;
open Constants;;
open Game_defs;;
open Mc_reachability;;
open Reachability_game;;


module BoolList_ResultType =
    struct 
        type t = bool list
        let equal x y = (x = y)
        let to_string l = 
            "[" ^ (String.concat "," (List.map string_of_bool l)) ^ "]"
    end;;

module MCTest = Test.Tester(BoolList_ResultType);;


let test_mc_safety () = 
    print_string "Testing MC Safety:\n\n";
    MCTest.run_tests ();
    print_string "Done.\n\n";;



(********************************************************)


let test1 () =
    let pds = new pds in
    pds#add_rule p_sbot_f_sbot;
    pds#add_rule p_a_p;
    pds#add_rule f_a_p_a;
    pds#add_rule f_a_f_aa;
    pds#add_rule f_sbot_f_sbot;
    let props = new property_map [(p, A, 1); (f, A, 2)] in
    let interesting_configs = [
        (f, word_sbot); (p, a_a_bot); (p, a_a_a_bot); (f, a_a_bot)
    ] in
    let game = new parity_game pds props interesting_configs in
    let (safety_game, _) = Conversions.pdg_to_pdrg game in
    (*print_string (safety_game#to_string);*)
    let mc = new mc_reachability safety_game in
    try
        let ma = mc#construct_winning_region in
        let results = mc#get_config_results ma in
        List.map snd (results#get_values)
    with x ->
        match x with
          Bad_input(es) -> 
            let errors = mc#input_error_list_to_string es in
            prerr_string ("Input errors: \n\n" ^ errors ^ "\n\n");
            []
        | _ -> [];;

let expected1 = 
    (* Eloise playing as Abe loses them all *)
    [false; false; false; false];;


MCTest.add_test "Test 1 -- full model check, Arnaud's example" test1 expected1;;


let test2 () =
    let pds = new pds in
    pds#add_rule p_sbot_f_sbot;
    pds#add_rule p_a_p;
    pds#add_rule f_a_p_a;
    pds#add_rule f_a_f_aa;
    pds#add_rule f_sbot_f_sbot;
    let props = new property_map [(p, A, 2); (f, A, 1)] in
    let interesting_configs = [
        (f, word_sbot); (p, a_a_bot); (p, a_a_a_bot); (f, a_a_bot)
    ] in
    let game = new parity_game pds props interesting_configs in
    let (safety_game, _) = Conversions.pdg_to_pdrg game in
    (*print_string (safety_game#to_string);*)
    let mc = new mc_reachability safety_game in
    let ma = mc#construct_winning_region in
    let results = mc#get_config_results ma in
    List.map snd (results#get_values);;
let expected2 = 
    (* Eloise playing as Abe wins them all *)
    [true; true; true; true];;

MCTest.add_test "Test 2 -- full model check, Arnaud's example negated" test2 expected2;;


let test3 () =
    let pds = new pds in
    pds#add_rule f_sbot_f_sbot;
    let props = new property_map [(f, A, 1)] in
    let interesting_configs = [(f, word_sbot)] in
    let game = new parity_game pds props interesting_configs in
    let (safety_game, _) = Conversions.pdg_to_pdrg game in
    (*print_string (safety_game#to_string);*)
    let mc = new mc_reachability safety_game in
    let ma = mc#construct_winning_region in
    let results = mc#get_config_results ma in
    List.map snd (results#get_values);;
let expected3 = 
    (* Eloise playing as Abe wins *)
    [true];;

MCTest.add_test "Test 3 -- simple odd loop" test3 expected3;;


let test4 () =
    let pds = new pds in
    pds#add_rule p_a_p;
    pds#add_rule p_a_f_b;
    pds#add_rule p_b_f_b;
    pds#add_rule f_b_p_a;
    pds#add_rule f_b_f;
    pds#add_rule f_b_f_a_b;
    let props = new property_map [(f, A, 1); (p, A, 1)] in
    let interesting_configs = 
        [(f, b_bot); (p, b_bot); (f, a_bot); (p, a_bot)] in
    let game = new parity_game pds props interesting_configs in
    let (safety_game, _) = Conversions.pdg_to_pdrg game in
    (*print_string (safety_game#to_string);*)
    let mc = new mc_reachability safety_game in
    let ma = mc#construct_winning_region in
    let results = mc#get_config_results ma in
    List.map snd (results#get_values);;
let expected4 = 
    (* results are reversed during construction -- not ideal to do this
     * manually *)
    [true; false; true; true];;

MCTest.add_test "Test 4 -- failed random test" test4 expected4;;


let test5 () =
    let pds = new pds in
    pds#add_rule f_b_p_b;
    pds#add_rule p_b_p;
    pds#add_rule p_b_p_a;
    pds#add_rule p_a_f;
    let props = new property_map [(f, E, 0); (p, A, 2)] in
    let interesting_configs = 
        [(p, a_bot); (f, a_bot); (p, b_bot); (f, b_bot)] in
    let game = new parity_game pds props interesting_configs in
    let (safety_game, _) = Conversions.pdg_to_pdrg game in
    (*print_string (safety_game#to_string);*)
    let mc = new mc_reachability safety_game in
    let ma = mc#construct_winning_region in
    let results = mc#get_config_results ma in
    List.map snd (results#get_values);;
let expected5 = 
    [true; true; true; true];;

MCTest.add_test "Test 5 -- failed random test" test5 expected5;;

let test6 () =
    let pds = new pds in
    pds#add_rule p_b_f;
    pds#add_rule p_b_f_b;
    pds#add_rule f_b_p_b_b;
    pds#add_rule f_b_p_b_a;
    pds#add_rule f_b_f;
    pds#add_rule f_a_p;
    let props = new property_map [(p, A, 1); (f, A, 0)] in
    let interesting_configs = 
        [(p, a_bot); (f, a_bot); (p, b_bot); (f, b_bot)] in
    let game = new parity_game pds props interesting_configs in
    let (safety_game, _) = Conversions.pdg_to_pdrg game in
    (*print_string (safety_game#to_string);*)
    let mc = new mc_reachability safety_game in
    let ma = mc#construct_winning_region in
    let results = mc#get_config_results ma in
    List.map snd (results#get_values);;
let expected6 = 
    [false; false; false; false];;

MCTest.add_test "Test 6 -- failed random test" test6 expected6;;


let test7 () =
    let pds = new pds in
    pds#add_rule p_b_f;
    pds#add_rule p_b_f_b;
    pds#add_rule f_b_p_b_b;
    let props = new property_map [(p, A, 1); (f, A, 0)] in
    let interesting_configs = [(p, b_bot)] in
    let game = new parity_game pds props interesting_configs in
    let (safety_game, _) = Conversions.pdg_to_pdrg game in
    (*print_string (safety_game#to_string);*)
    let mc = new mc_reachability safety_game in
    let ma = mc#construct_winning_region in
    let results = mc#get_config_results ma in
    List.map snd (results#get_values);;
let expected7 = 
    [false];;

MCTest.add_test "Test 7 -- test 6 distilled" test7 expected7;;

let test8 () =
    let pds = new pds in
    pds#add_rule f_b_f_b;
    pds#add_rule f_b_f_a_b;
    pds#add_rule f_b_p_b;
    pds#add_rule f_a_p_a_b;
    pds#add_rule p_b_f_a;
    let props = new property_map [(p, E, 0); (f, E, 0)] in
    let interesting_configs = 
        [(p, a_bot); (f, a_bot); (p, b_bot); (f, b_bot)] in
    let game = new parity_game pds props interesting_configs in
    let (safety_game, _) = Conversions.pdg_to_pdrg game in
    (*print_string (safety_game#to_string);*)
    let mc = new mc_reachability safety_game in
    let ma = mc#construct_winning_region in
    let results = mc#get_config_results ma in
    List.map snd (results#get_values);;
let expected8 = 
    [false; true; true; true];;

MCTest.add_test "Test 8 -- failed random test" test8 expected8;;


let test9 () =
    let pds = new pds in
    pds#add_rule f_b_f_b;
    pds#add_rule f_b_f_a_b;
    pds#add_rule f_b_p_b;
    pds#add_rule f_a_p_a_b;
    pds#add_rule p_b_f_a;
    let props = new property_map [(p, E, 0); (f, E, 0)] in
    let interesting_configs = 
        [(f, a_bot)] in
    let game = new parity_game pds props interesting_configs in
    let (safety_game, _) = Conversions.pdg_to_pdrg game in
    (*print_string (safety_game#to_string);*)
    let mc = new mc_reachability safety_game in
    let ma = mc#construct_winning_region in
    let results = mc#get_config_results ma in
    List.map snd (results#get_values);;
let expected9 = 
    [true];;

MCTest.add_test "Test 9 -- test 8 distilled" test9 expected9;;


