
open Mc_pds_mucalc;;
open Mc_parity;;


module List_ResultType =
    struct 
        type t = bool list * bool list
        let equal x y = (x = y)
        let to_string (x, y) =
            let string_list l = 
                List.fold_left (fun s b -> s ^ " " ^ (string_of_bool b)) 
                               "" 
                               l in
            string_list x ^ ", " ^ string_list y
    end;;


module MuConfigsTest = Test.Tester(List_ResultType);;

let run_mu_config_test problem =
    let lexbuf = Lexing.from_string problem in
    let pdmu = Parser.pds_mucalc Lexer.token lexbuf in 
    let mcmu = new mc_pds_mucalc pdmu in
    let mamu = mcmu#construct_denotation in
    let pdg = Conversions.pdmu_to_pdg pdmu in
    let mcg = new mc_parity pdg in
    let mag = mcg#construct_winning_region in
    (List.map snd ((mcmu#get_config_results mamu)#get_values),
     List.map snd ((mcg#get_config_results mag)#get_values));;

(* to avoid having to repeat lists for expected values *)
let pair_of x = (x, x)

let test_configs () = 
    print_string "Testing Configs:\n\n";
    MuConfigsTest.run_tests ();
    print_string "Done.\n\n";;

let test1 () =
    run_mu_config_test "
        Rules:

        p a -> p _;
        p # -> f #;
        f # -> f #;
        f a -> f a a;
        f a -> p a;

        Interesting Configurations:

        p a a a #;
        p a a #;

        Mu Property:

        mu Z1 . nu Z2 . ((p & []Z1) | (f & []Z2))

        Propositions:

        p a p;
        p # p;
        f a f;
        f # f;
    ";;

let expected1 = pair_of [true; true];;

MuConfigsTest.add_test "Test 1 -- Journal version interesting configs" 
                       test1 
                       expected1;;

let test2 () =
    run_mu_config_test "
        Rules:

        p a -> p _;

        Interesting Configurations:

        p a a #;
        p a #;

        Mu Property:

        <@p><@p>p

        Propositions:

        p a p;
        p # p;
    ";;

let expected2 = pair_of [true; false];;

MuConfigsTest.add_test "Test 2 -- Two steps forward" 
                       test2 
                       expected2;;

let test3 () =
    run_mu_config_test "
        Rules:

        p a -> p _;
        p b -> p a;
        f a -> p b;

        Interesting Configurations:

        p #;
        p a #;

        Mu Property:

        ~[@p]~[@p]p

        Propositions:

        p a p;
        p b p;
        p # p;
    ";;

let expected3 = pair_of [true; true];;

MuConfigsTest.add_test "Test 3 -- Two steps back" 
                       test3 
                       expected3;;

let test4 () =
    run_mu_config_test "
        Rules:

        p a -> p _;
        p b -> p a;
        f a -> p b;

        Interesting Configurations:

        p #;
        p a #;

        Mu Property:

        ~[@p]~[]p

        Propositions:

        p a p;
        p b p;
        p # p;
    ";;

let expected4 = pair_of [true; false];;

MuConfigsTest.add_test "Test 4 -- Two steps back, second unconstrained" 
                       test4 
                       expected4;;

let test5 () =
    run_mu_config_test "
        Rules:

        p a -> p _;
        p b -> p a;
        f a -> p b;

        Interesting Configurations:

        p #;
        p a #;

        Mu Property:

        ~[@p]~<\\p>p

        Propositions:

        p a p;
        p b p;
        p # p;
    ";;

let expected5 = pair_of [false; false];;

MuConfigsTest.add_test "Test 5 -- Two steps back, second prevents" 
                       test5 
                       expected5;;

let test6 () =
    run_mu_config_test "
        Rules:

        p b -> p a;
        f a -> p a;
        f a -> p b;

        Interesting Configurations:

        p a #;
        p b #;

        Mu Property:

        nu X . <@p>~<\\p><> X

        Propositions:

        p a p;
        p b p;
        p # p;
    ";;

let expected6 = pair_of [false; true];;

MuConfigsTest.add_test "Test 6 -- loops back and forth" 
                       test6 
                       expected6;;

