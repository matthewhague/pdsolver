
open Tools;;

module Bool_ResultType =
    struct 
        type t = bool
        let equal x y = (x = y)
        let to_string = string_of_bool
    end;;

module MiscTest = Test.Tester(Bool_ResultType);;

let test_misc () = 
    print_string "Testing Misc:\n\n";
    MiscTest.run_tests ();
    print_string "Done.\n\n";;

(********************************************************)

(* borrowed from * http://rosettacode.org/wiki/Least_common_multiple#Oforth *)
let rec gcd u v =
  if v <> 0 then (gcd v (u mod v))
  else (abs u)

let lcm m n =
    match m, n with
    | 0, _ | _, 0 -> 0
    | m, n -> abs (m * n) / (gcd m n)

(* mine *)
let rec lcm_list l = 
    match l with
      [] -> 1
    | h::t -> lcm h (lcm_list t)

let do_test n = 
    let ps = primes_over_bound n in
    (lcm_list ps) >= n;; 

let test1 () = do_test 34;;
let test2 () = do_test 85;; 
let test3 () = do_test 390;; 

MiscTest.add_test "Test 1 -- prime bound 34" test1 true;;
MiscTest.add_test "Test 2 -- prime bound 85" test2 true;;
MiscTest.add_test "Test 3 -- prime bound 390" test3 true;;

