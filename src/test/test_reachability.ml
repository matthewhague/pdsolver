

open Mc_reachability;;
open Conversions;;

module List_ResultType =
    struct 
        type t = bool list
        let equal x y = (x = y)
        let to_string x =
            let string_list l = 
                List.fold_left (fun s b -> s ^ " " ^ (string_of_bool b)) 
                               "" 
                               l in
            string_list x
    end;;


module ReachabilityTest = Test.Tester(List_ResultType);;

(* TODO: test games to mu conversion *)

let run_reach_test problem =
    let lexbuf = Lexing.from_string problem in
    let pdrg = Parser.reachability_game Lexer.token lexbuf in 
    let mc = new mc_reachability pdrg in
    let ma = mc#construct_winning_region in
    List.map snd ((mc#get_config_results ma)#get_values);;

let run_mu_reach_test problem =
    let lexbuf = Lexing.from_string problem in
    let pdmu = Parser.pds_mucalc Lexer.token lexbuf in 
    let pdrg = Conversions.pdmu_to_pdrg pdmu in
    let mc = new mc_reachability pdrg in
    let ma = mc#construct_winning_region in
    List.map snd ((mc#get_config_results ma)#get_values);;


let test_reachability () = 
    print_string "Testing reachability games:\n\n";
    ReachabilityTest.run_tests ();
    print_string "Done.\n\n";;

let test1 () =
    run_reach_test "
        Rules:

        p # -> f #;
        p a -> p _;
        f a -> p a;
        f a -> f a a;
        f # -> f #;

        Control States:

        p Abelard;
        f Abelard;

        Target Heads:

        f #;

        Interesting Configurations:

        p a a a #;
        p #;
        f a #;
        f #;
    ";;

ReachabilityTest.add_test "Test 1 -- Journal version interesting configs" 
                          test1 
                          [true; true; false; true];;

let test2 () =
    run_reach_test "
        Rules:

        p # -> f #;
        p a -> p _;
        f a -> p a;
        f a -> f a a;
        f # -> f #;

        Control States:

        p Eloise;
        f Eloise;

        Target Heads:

        f #;

        Interesting Configurations:

        p a a a #;
        p #;
        f a #;
        f #;
    ";;

ReachabilityTest.add_test "Test 2 -- Journal version all Eloise" 
                          test2 
                          [true; true; true; true];;


let test3 () =
    run_mu_reach_test "
        Rules:

        p # -> f #;
        p a -> p _;
        f a -> p a;
        f a -> f a a;
        f # -> f #;

        Mu Property:

        mu Z . (fbot | []Z)

        Propositions:

        f # fbot;

        Interesting Configurations:

        p a a a #;
        p #;
        f a #;
        f #;
    ";;

ReachabilityTest.add_test "Test 3 -- mu calc version of test 1" 
                          test1 
                          [true; true; false; true];;

let test4 () =
    run_mu_reach_test "
        Rules:

        p # -> f #;
        p a -> p _;
        f a -> p a;
        f a -> f a a;
        f # -> f #;

        Mu Property:

        mu Z . (fbot | <>Z)

        Propositions:

        f # fbot;

        Interesting Configurations:

        p a a a #;
        p #;
        f a #;
        f #;
    ";;

ReachabilityTest.add_test "Test 4 -- mu calc version of test 2" 
                          test4 
                          [true; true; true; true];;

let test5 () =
    run_reach_test "
        Rules:

        p a -> f b;
        p a -> f b a;
        p a -> f _;
        p # -> p a #;
        f b -> p a;
        f b -> p a b;
        f b -> p _;

        Control States:

        p Eloise;
        f Abelard;

        Target Heads:

        f a;

        Interesting Configurations:

        p a a #;
        f b #;
    ";;

ReachabilityTest.add_test "Test 5 -- All Three Rules Per Head" 
                          test5 
                          [true; false];;

let test6 () =
    run_reach_test "
        Rules:

        p a -> f b;
        p a -> f b a;
        p a -> f _;
        p # -> p a #;
        f b -> p a;
        f b -> p a b;
        f b -> p _;
        p a -> p _;
        p b -> p _;

        Control States:

        p Eloise;
        f Abelard;

        Target Heads:

        p #;

        Interesting Configurations:

        p a a #;
        f b #;
    ";;

ReachabilityTest.add_test "Test 6 -- All Three Rules Per Head Positive" 
                          test6 
                          [true; true];;

let test7 () =
    run_reach_test "
        Rules:

        p a -> p _;
        p a -> f b;
        p a -> f _;
        f b -> p a;
        f b -> p _;

        Control States:

        p Eloise;
        f Abelard;

        Target Heads:

        p #;

        Interesting Configurations:

        p a #;
    ";;

ReachabilityTest.add_test "Test 7 -- Test 6 simplified" 
                          test7
                          [true];;



