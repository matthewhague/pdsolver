open Pds;;
open Words;;
open Reachability_game;;
open Ma;;
open State;;
open Mc;;
open Game_defs;;




type pdrg_error = BadBotUse of rule list | 
                  NeedsBot | 
                  OwnersMissing of control_state list |
                  BadInterestingConfig of (config * CharacterSet.t) |
                  PushTooMuch of rule list;;
type pdrg_errors = None | Errors of pdrg_error list;;


(* We use QVal(pstar) instead of QStar since we introduce rules that use
 * internal states as part of the algorithm
 *
 * Note: we need to reset these before algorithm starts else we clash with
 * others with reset_special_controls
 *)
let pstar = ref (make_control_state "*pstar")
let pepsilon = ref (make_control_state "*pepsilon")

module BasicState =
    struct 
        type t = control_state
        let compare = compare
        let to_string = control_state_to_string
        let to_dot_string = control_state_to_string
        let can_simulate p p' = 
          if p = !pstar then
              not (p' = !pepsilon)
          else
              p = p'
    end;;

module ReachMa = Ma.Make(BasicState);;
module ReachState = ReachMa.MaState;;

type alt_tran = ReachState.state * character * ReachState.StateSet.t

let alt_tran_to_string (p, a, s) =
    (control_state_to_string p) ^ " " ^
    (character_to_string a) ^ " " ^
    (ReachState.StateSet.to_string s)

(* For maintaining a worklist of new ma transitions *)
module OrderedAltTran = 
    struct
        type t = alt_tran
        let compare = compare
        let to_string (q, a, s) =  
            (ReachState.state_to_string q) ^ " -" ^
            (character_to_string a) ^ "-> " ^
            (ReachState.StateSet.to_string s)
    end;;

module TranSet = Extset.Make(OrderedAltTran);;


(* Alt rules include the "extra" ma states that should be added to any
 * transition introduced because of them (the part played by F in Schwoon et al
 *)
type alt_rule = (control_state * character * NextSet.t * ReachState.StateSet.t);;

let alt_rule_to_string (p, a, ns, xs) =
    "(" ^
    (control_state_to_string p) ^ " " ^
    (character_to_string a) ^ " " ^
    (NextSet.to_string ns) ^ " " ^
    (ReachState.StateSet.to_string xs) ^
    ")"

module OrderedAltRule =
    struct 
        type t = alt_rule
        let compare = compare
        let to_string = alt_rule_to_string
    end;;

module HashAltRule =
    struct 
        type t = alt_rule
        let compare = compare
        let hash = Hashtbl.hash
        let equal (p, a, ns, xs) (p', a', ns', xs') =
            (control_states_equal p p' &&
             characters_equal a a' &&
             NextSet.equal ns ns' &&
             ReachState.StateSet.equal xs xs')
    end;;

module AltRuleSet = Extset.Make(OrderedAltRule);;
module AltRuleHashtbl = Hashtbl.Make(HashAltRule);;





class mc_reachability_stats (ma_nstates : int) (ma_ntrans : int)  
                            (ma_max_states : int) (ma_max_trans : int)  
                            (stats_comp_time : float) (total_time : float) 
                            (final_opt_time : float) (pdrg_stats : pdrg_stats) =
    object
        method get_ma_nstates = ma_nstates
        method get_ma_ntrans = ma_ntrans
        method get_ma_max_states = ma_max_states
        method get_ma_max_trans = ma_max_trans
        method get_stats_comp_time = stats_comp_time
        method get_total_time = total_time
        method get_check_time = total_time -. stats_comp_time
        method get_final_opt_time = final_opt_time
        method get_pdrg_stats = pdrg_stats

        method to_string =
            "Total time: " ^ (string_of_float total_time) ^ "  " ^
            "Check time: " ^ (string_of_float (total_time -. stats_comp_time)) ^ "\n" ^
            "Gather stats time: " ^ (string_of_float stats_comp_time) ^ "  " ^
            "Final opt time: " ^ (string_of_float final_opt_time) ^ "\n" ^
            "Ma states: " ^ (string_of_int ma_nstates) ^ "  " ^
            "Ma trans: " ^ (string_of_int ma_ntrans) ^ "\n" ^
            "Max states: " ^ (string_of_int ma_max_states) ^ "  " ^
            "Max trans: " ^ (string_of_int ma_max_trans) ^ "\n" ^
            pdrg_stats#to_string

        method to_row_string =
            (string_of_float total_time) ^ " " ^
            (string_of_float (total_time -. stats_comp_time)) ^ " " ^
            (string_of_float stats_comp_time) ^ " " ^
            (string_of_float final_opt_time) ^ " " ^
            (string_of_int ma_nstates) ^ " " ^
            (string_of_int ma_ntrans) ^ " " ^
            (string_of_int ma_max_states) ^ " " ^
            (string_of_int ma_max_trans) ^ " " ^
            pdrg_stats#to_row_string

    end;;

   
    

exception Bad_input of pdrg_error list

class mc_reachability (pdrg : reachability_game) =
    object (self)
        val pdrg = pdrg

        val mutable disable_hard_opt = false
        val mutable prod_union_opt = false
        val mutable gather_stats = false

        method private reset_special_controls =
            pstar := make_control_state "*pstar";
            pepsilon := make_control_state "*pepsilon"

        (* for testing purposes only *)
        method get_pdrg = pdrg

        method set_disable_hard_opt ho = 
            disable_hard_opt <- ho
        method get_disable_hard_opt = disable_hard_opt

        method set_prod_union_opt o = 
            prod_union_opt <- o
        method get_prod_union_opt = prod_union_opt

        val mutable max_states = 0
        val mutable max_trans = 0
        val mutable stats_time = 0.0
        val mutable check_time = 0.0
        val mutable final_opt_time = 0.0


        method set_gather_stats v = 
            gather_stats <- v

        method private reset_stats = 
            check_time <- 0.0;
            max_states <- 0;
            max_trans <- 0;
            stats_time <- 0.0;
            final_opt_time <- 0.0


        method private gather_stats ma = 
            if gather_stats then (
                let start_time = Sys.time () in
                max_states <- max max_states ma#num_states;
                max_trans <- max max_trans ma#num_trans;
                stats_time <- stats_time +. (Sys.time ()) -. start_time
            )


        method get_stats (ma : ReachMa.ma) = 
            new mc_reachability_stats ma#num_states
                                      ma#num_trans
                                      max_states
                                      max_trans
                                      stats_time
                                      check_time
                                      final_opt_time
                                      pdrg#get_stats


        method private assert_good_input =
            match self#checkable_errors with
              None -> ()
            | Errors(es) -> raise (Bad_input(es))


        method checkable_errors = 
            let game_rules = pdrg#get_pds#get_all_rules in
            let alphabet = pdrg#get_pds#get_alphabet in
            let controls = pdrg#get_pds#get_control_states in
            let missing_owners = 
                let do_p p missed = 
                    try 
                        let _ = pdrg#get_owner p in
                        missed
                    with e ->
                        p::missed in
                let missed = ControlStateSet.fold do_p controls [] in
                if (missed = []) then [] else [OwnersMissing(missed)] in
            let bottom_use_errors =
                let do_rule r errors = 
                    let (_, a, _, w) = rule_tuple r in
                    let bad = (* moves bottom from bottom *)
                              ((characters_equal a sbot) && (not (word_ends_with w sbot))) ||
                              (* adds an extra bottom *)
                              ((not (characters_equal a sbot)) && (word_contains w sbot)) in
                    if bad then r::errors else errors in
                let bad_rules = RuleSet.fold do_rule game_rules [] in
                if (bad_rules = []) then [] else [BadBotUse(bad_rules)] in
            let needs_bottom_errors = 
                if not (CharacterSet.exists (characters_equal sbot) alphabet) then
                    [NeedsBot]
                else [] in
            let bad_interesting_config_errors = 
                let do_config errs (p, w) = 
                    let w_chars = word_get_characters w in
                    let strays = CharacterSet.diff w_chars alphabet in
                    if CharacterSet.is_empty strays then
                        errs
                    else
                        (BadInterestingConfig((p, w), strays))::errs in
                List.fold_left do_config [] (pdrg#get_interesting_configs) in
            let push_too_much_errors = 
                let do_rule r errors = 
                    let (_, _, _, w) = rule_tuple r in
                    if (word_length w) > 2 then r::errors else errors in
                let bad_rules = RuleSet.fold do_rule game_rules [] in
                if (bad_rules = []) then [] else [PushTooMuch(bad_rules)] in
            let errors = List.concat [bottom_use_errors; 
                                      needs_bottom_errors; 
                                      missing_owners;
                                      bad_interesting_config_errors;
                                      push_too_much_errors] in
            if (errors = []) then None else (Errors(errors))

                

        method private init_ma ma = 
            (* Add States *)
            let in_states = ReachState.StateSet.from_list [!pstar; !pepsilon] in
            let init_states = 
                ControlStateSet.fold (fun p states -> 
                                          ReachState.StateSet.add p states)
                                     pdrg#get_pds#get_control_states 
                                     ReachState.StateSet.empty in
            ma#add_states in_states;
            ma#add_states init_states;
            ma#set_inits init_states;
            ma#set_finals (ReachState.StateSet.singleton !pepsilon);
            (* Transitions added to the worklist in main algorithm loop, rather
             * than directly here
             *)



        method construct_winning_region = 
            Progress_out.pick_spinner ();
            Progress_out.print_string "Initialising...";
            self#reset_special_controls;
            self#reset_stats;
            self#assert_good_input;
            let start_time = Sys.time () in
            let ma = new ReachMa.ma in
            self#init_ma ma;
            Progress_out.erase_string "Initialising...";
            Progress_out.print_string "Computing winning region: ";
            self#compute_fixed_point ma;
            let done_time = Sys.time () in
            Progress_out.erase_string "Computing winning region: ";
            Progress_out.print_string "Optimising automaton...";
            ma#remove_unreachable;
            let opted_time = Sys.time () in
            check_time <- done_time -. start_time;
            final_opt_time <- opted_time -. done_time;
            Progress_out.erase_string "Optimising automaton...";
            Progress_out.print_string "Done.";
            Progress_out.print_newline ();
            ma



        method optimise_automaton : ReachMa.ma -> unit =
            fun ma -> 
                let tran_opt t = 
                    let (q, a, ss) = ReachMa.ht_tuple t in
                    let opt_ss = (ReachState.optimise_hyper_state_set ss) in
                    (`NewTran(ReachMa.make_ht q a opt_ss)) in
                ma#self_map tran_opt;
                


        method private compute_fixed_point ma =
            Progress_out.print_string "Processing Pops...";
            let (rules, trans) = self#process_pops ma in
            Progress_out.erase_string "Processing Pops...";

            let worklist = ref TranSet.empty in
            let num_rules = (AltRuleSet.cardinal rules) in
            let rule_back_map = HeadHashtbl.create num_rules in
            let rule_fwd_map = HeadHashtbl.create num_rules in
            let done_trans = ReachMa.MaHeadHashtbl.create num_rules in

            let has_done_tran (q, a, s) =
                if ReachMa.MaHeadHashtbl.mem done_trans (q, a) then
                    let ss = ReachMa.MaHeadHashtbl.find done_trans (q, a) in
                    ReachState.SetOfStateSets.exists (fun s' ->
                        ReachState.StateSet.subset s' s
                    ) ss
                else false in

            let add_done_tran (q, a, s) =
                if ReachMa.MaHeadHashtbl.mem done_trans (q, a) then
                    let ss = ReachMa.MaHeadHashtbl.find done_trans (q, a) in
                    let ss' = ReachState.SetOfStateSets.add s ss in
                    ReachMa.MaHeadHashtbl.replace done_trans (q, a) ss'
                else 
                    let ss = ReachState.SetOfStateSets.singleton s in
                    ReachMa.MaHeadHashtbl.add done_trans (q, a) ss in

            let has_rule (p, a, ns, xs) =
                if HeadHashtbl.mem rule_fwd_map (p, a) then
                    let rules = HeadHashtbl.find rule_fwd_map (p, a) in
                    AltRuleSet.exists (fun (_, _, ns', xs') ->
                        NextSet.subset ns' ns &&
                        ReachState.StateSet.subset xs' xs
                    ) rules
                else false in

            let get_rules_back p a =
                if HeadHashtbl.mem rule_back_map (p, a) then
                    HeadHashtbl.find rule_back_map (p, a)
                else
                    AltRuleSet.empty in

            let add_rule_fwd (p, a, ns, xs) =
                if HeadHashtbl.mem rule_fwd_map (p, a) then
                    let rs = HeadHashtbl.find rule_fwd_map (p, a) in
                    let rs' = AltRuleSet.add (p, a, ns, xs) rs in
                    HeadHashtbl.replace rule_fwd_map (p, a) rs'
                else
                    let rs = AltRuleSet.singleton (p, a, ns, xs) in 
                    HeadHashtbl.add rule_fwd_map (p, a) rs in

            let add_rule_back p b r =
                if HeadHashtbl.mem rule_back_map (p, b) then
                    let rs = HeadHashtbl.find rule_back_map (p, b) in
                    let rs' = AltRuleSet.add r rs in
                    HeadHashtbl.replace rule_back_map (p, b) rs'
                else
                    let rs = AltRuleSet.singleton r in 
                    HeadHashtbl.add rule_back_map (p, b) rs in
          
            let add_tran_worklist (q, a, s) = 
                worklist := TranSet.add (q, a, s) !worklist in

            let rec tran_vs_rule_split (q, a, s) p b p' w ns xs =
                if (word_length w) = 1 then
                    let sall = ReachState.StateSet.union s xs in
                    if (NextSet.cardinal ns) = 0 then
                        add_tran_worklist (p, b, sall)
                    else
                        add_rule (p, b, ns, sall)
                else ( 
                    (* word len is 2 *)
                    let c = word_head (word_tail w) in 
                    let ns' = 
                        ReachState.StateSet.fold (fun q ns' ->
                            NextSet.add (q, make_word [c]) ns'
                        ) s ns in
                    add_rule (p, b, ns', xs)
                )

            and tran_vs_rule (q, a, s) (p, b, ns, xs) = 
                NextSet.iter (fun (p', w) ->
                    let a' = word_head w in
                    if (p' = q) && (characters_equal a a') then
                        let ns' = NextSet.remove (p', w) ns in
                        tran_vs_rule_split (q, a, s) p b p' w ns' xs
                    else ()
                ) ns

            and process_by_old (p, a, ns, xs) = 
                NextSet.iter (fun (p', w) ->
                    let b = word_head w in
                    if ReachMa.MaHeadHashtbl.mem done_trans (p', b) then
                        let trans = 
                            ReachMa.MaHeadHashtbl.find done_trans (p', b) in
                        ReachState.SetOfStateSets.iter (fun s -> 
                            tran_vs_rule (p', b, s) (p, a, ns, xs)
                        ) trans
                    else ()
                ) ns

            and add_rule (p, a, ns, xs) =
                if not (has_rule (p, a, ns, xs)) then (
                    add_rule_fwd (p, a, ns, xs);
                    NextSet.iter (fun (p', w) ->
                        let b = word_head w in
                        add_rule_back p' b (p, a, ns, xs)
                    ) ns;
                    process_by_old (p, a, ns, xs)
                ) else () in

            Progress_out.print_string "Creating initial rule maps...";
            AltRuleSet.iter (fun r -> add_rule r) rules;
            Progress_out.erase_string "Creating initial rule maps...";

            Progress_out.print_string "Initialising worklist...";
            (* Add trans from pops *)
            TranSet.iter add_tran_worklist trans;
            (* Add Internal Transitions *)
            CharacterSet.foreach pdrg#get_pds#get_alphabet (fun a -> 
                if characters_equal a sbot then
                    add_tran_worklist (!pstar, 
                                       sbot,
                                       ReachState.StateSet.singleton !pepsilon)
                else
                    add_tran_worklist (!pstar,
                                       a,
                                       ReachState.StateSet.singleton !pstar)
            );
            (* Target head transitions *)
            HeadSet.foreach pdrg#get_target_heads (fun (p, a) ->
                if characters_equal a sbot then
                    add_tran_worklist (p,
                                       sbot,
                                       (ReachState.StateSet.singleton !pepsilon))
                else
                    add_tran_worklist (p,
                                       a,
                                       (ReachState.StateSet.singleton !pstar))
            );
            Progress_out.erase_string "Initialising worklist...";

            let spinner = new Progress_out.spinner in
            spinner#start_spinner;

            let last_spin_time = ref (Sys.time ()) in
            while (not (TranSet.is_empty !worklist)) do
                if (Sys.time ()) > (!last_spin_time +. 2.0) then (
                    spinner#update_spinner;
                    last_spin_time := Sys.time ();
                );

                let (q, a, s) = TranSet.choose !worklist in
                worklist := TranSet.remove (q, a, s) !worklist;

                if not (has_done_tran (q, a, s)) then (
                    (* add to done first else new rules will miss it! *)
                    add_done_tran (q, a, s);
                    let rules = get_rules_back q a in
                    AltRuleSet.iter (fun r ->
                        tran_vs_rule (q, a, s) r 
                    ) rules;
                ) else ()
            done;
            spinner#clear_spinner;

            Progress_out.print_string "Copying done_trans to ma...";
            ReachMa.MaHeadHashtbl.iter (fun (q, a) ss ->
                ma#add_hyper_transition (ReachMa.make_ht q a ss)
            ) done_trans;
            Progress_out.erase_string "Copying done_trans to ma...";



        (*
         * ma -- automaton, add transitions to for complete pop rules
         * return (new_rules, trans, extra_states) where
         *      new_rules is a set of alt_rules that should be used in the rest
         *                of the construction
         *      trans is a TranSet of transitions to be added due to pops
         *      extra_states is a altrulehashtbl from rules to sets of states that should
         *                   accompany any new transitions (F in Schwoon et al)
         *)
        method private process_pops ma = 
            let pds = pdrg#get_pds in
            (* initial worklist combines all rules for A and just copies across
             * those for E
             *)
            let empty_xs = ReachState.StateSet.empty in
            let worklist = 
                pds#heads_next_fold (fun p a ns rs ->
                    if pdrg#get_owner p == A then
                        AltRuleSet.add (p, a, ns, empty_xs) rs
                    else
                        NextSet.fold (fun (p', w) rs ->
                            let next = NextSet.singleton (p', w) in
                            AltRuleSet.add (p, a, next, empty_xs) rs
                        ) ns rs
                ) AltRuleSet.empty in
            let rules = ref (AltRuleSet.empty) in
            let trans = ref (TranSet.empty) in
            AltRuleSet.iter (fun alt_rule -> 
                let (p, a, ns, xs) = alt_rule in
                let is_pop (p', w) = word_is_empty w in
                let pops = NextSet.filter is_pop ns in
                let ns' = NextSet.diff ns pops in
                let qps = NextSet.fold (fun (p', _) qps ->
                    ReachState.StateSet.add p' qps
                ) pops ReachState.StateSet.empty in

                if NextSet.is_empty ns' then (
                    trans := TranSet.add (p, a, qps) !trans
                ) else (
                    let new_rule = (p, a, ns', qps) in
                    rules := AltRuleSet.add new_rule !rules;
                )
            ) worklist;
            (!rules, !trans)


        method input_error_to_string e =
            match e with
              BadBotUse(rules) -> 
                List.fold_left (fun s r -> s ^ (rule_to_string r) ^ "\n") 
                               "Rules using stack bottom incorrectly:\n\n"
                               rules
            | NeedsBot -> "Pushdown system does not have the bottom of stack symbol"
            | OwnersMissing(cs) -> 
                List.fold_left (fun s p -> s ^ (control_state_to_string p) ^ " ")
                               "The following control states are not assigned owners and colours:\n\n"
                               cs
            | BadInterestingConfig(c, chars) -> ("Interesting config '" ^ (config_to_string c) ^ "' " ^
                                                 "has characters " ^ (CharacterSet.to_string chars) ^ " " ^
                                                 "that do not appear in the PDG.")
            | PushTooMuch(rules) ->
                List.fold_left (fun s r -> s ^ (rule_to_string r) ^ "\n") 
                               "Rules pushing more than two characters onto stack:\n\n"
                               rules


        method input_error_list_to_string es =
            List.fold_left (fun s e -> s ^ (self#input_error_to_string e) ^ "\n")
                           ""
                           es

 
        method get_config_results (ma : ReachMa.ma) = 
            let do_config res c = 
                let truth = self#get_config_result ma c in
                (c, truth)::res in
            let res = List.fold_left do_config [] (pdrg#get_interesting_configs) in
            new interesting_config_results res

        method get_config_result (ma : ReachMa.ma) (c : config) = 
            let (p, w) = c in
            ma#accepts p w


    end;;


