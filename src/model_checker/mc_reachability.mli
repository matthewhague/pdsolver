open Reachability_game;;
open Ma;;
open State;;
open Words;;
open Pds;;
open Mc;;


module ReachMa : Ma.S with type state = control_state



type pdrg_error = BadBotUse of rule list | 
                  NeedsBot | 
                  OwnersMissing of control_state list |
                  BadInterestingConfig of (config * CharacterSet.t) |
                  PushTooMuch of rule list;;
type pdrg_errors = None | Errors of pdrg_error list

exception Bad_input of pdrg_error list


class mc_reachability_stats : 
    int -> int -> int -> int -> float -> float -> float -> pdrg_stats ->
    object
        method get_ma_nstates : int
        method get_ma_ntrans : int
        method get_ma_max_states : int
        method get_ma_max_trans : int
        method get_stats_comp_time : float
        method get_total_time : float
        method get_check_time : float
        method get_final_opt_time : float
        method get_pdrg_stats : pdrg_stats

        method to_string : string
        method to_row_string : string
    end



class mc_reachability : 
    reachability_game ->
    object
        method construct_winning_region : ReachMa.ma

        method checkable_errors : pdrg_errors

        method optimise_automaton : ReachMa.ma -> unit

        method input_error_to_string : pdrg_error -> string
        method input_error_list_to_string : pdrg_error list -> string

        method get_pdrg : reachability_game 

        method set_disable_hard_opt : bool -> unit
        method get_disable_hard_opt : bool
        method set_prod_union_opt : bool -> unit
        method get_prod_union_opt : bool

        method set_gather_stats : bool -> unit
        method get_stats : ReachMa.ma -> mc_reachability_stats

        method get_config_results : ReachMa.ma -> interesting_config_results
        method get_config_result : ReachMa.ma -> config -> bool
    end;;




