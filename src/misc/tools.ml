
let rec range_iter min max f =
    if min <= max then (
        f min;
        range_iter (min+1) max f
    );;

(* range_fold_left : ('a -> 'b -> 'a) -> 'a -> min -> max -> 'a *)
let rec range_fold_left f init min max =
    if min > max then
        init
    else
        let init' = f init min in
        range_fold_left f init' (min+1) max;;

(* returns a list of prime numbers [p1,...,pn] such that 
 * p1 * ... * pn > n.
 *
 * NOTE: does not necessarily start at 2, starts at MIN_PRIME *)
let primes_over_bound n = 
    let is_prime below_primes n = 
        not (List.exists (fun p ->
            n mod p = 0
        ) below_primes) in
    let rec do_loop n below_primes prod_primes product next_p =
        if product >= n then
            (prod_primes, product)
        else if is_prime below_primes next_p then
            let below_primes' = List.cons next_p below_primes in
            let prod_primes' = List.cons next_p prod_primes in
            let product' = product * next_p in
            do_loop n below_primes' prod_primes' product' (next_p + 1) 
        else
            do_loop n below_primes prod_primes product (next_p + 1) in
    let min_prime = 2 in
    let below_primes = [2] in
    let (primes, primes_prod) = 
        do_loop n below_primes [min_prime] min_prime (min_prime + 1) in
    (* because of way loop works, higher primes are at head 
     * filter them to remove as many large unneeded ones as possible.
     * E.g. 5,7,9,11 may be too big and 7,11 may be enough *)
    let rec filter_primes low_primes low_prod keep_primes keep_prod =
        match low_primes with
          [] -> keep_primes
        | max_p::lower_primes ->
            let lower_prod = low_prod / max_p in
            if lower_prod * keep_prod >= n then
                filter_primes lower_primes lower_prod keep_primes keep_prod
            else
                let keep_primes' = max_p::keep_primes in
                let keep_prod' = keep_prod * max_p in
                filter_primes lower_primes lower_prod keep_primes' keep_prod' in
    filter_primes primes primes_prod [] 1;;
