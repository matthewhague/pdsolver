
open Pds;;

type owner = A | E;;

val owner_to_string : owner -> string;;
(* A -> E, E -> A *)
val opp_owner : owner -> owner;;

(* For making maps from control states to properties *)
module OrderedControlState :
    sig
        type t = control_state
        val compare : t -> t -> int
    end;;

