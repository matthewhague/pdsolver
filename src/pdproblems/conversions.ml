
open Pds;;
open Pds_mucalc;;
open Parity_game;;
open Reachability_game;;
open Mucalc;;
open Words;;
open Game_defs;;
open Tools;;

module OrderedFmla = 
    struct
        type t = mu_formula
        let compare = compare
    end;;

module FmlaParityMap = Map.Make(OrderedFmla);;

let mu_to_pdg_control p f = 
    make_control_state ((control_state_to_string p) ^ "__" ^ (mu_formula_to_string f));;

let mu_to_pdg_control_back b p a f =
    make_control_state ((character_to_string b) ^
                        "__" ^         
                        (control_state_to_string p) ^ 
                        "__" ^
                        (character_to_string a) ^
                        "__" ^
                        (mu_formula_to_string f));;

(* adds rules for a pushdown game based on a mucalc formula
 *      pds -- the pds
 *      subs -- set of subformulas
 *      get_sat_heads -- a function returning the set of heads that satisfy a
 *                       set of propositions (pdmu#get_sat_heads)
 *      psat -- a winning control state
 *      punsat -- a losing control state
 *      g_pds -- game to add rules to
 *)
let add_rules pds subs get_sat_heads psat punsat g_pds = 
    let controls = pds#get_control_states in
    let alphabet = pds#get_alphabet in

    let add_state_change_rules p p' = 
        CharacterSet.foreach alphabet (fun c ->
            let rule = make_rule p c p' (make_word [c]) in
            g_pds#add_rule rule
        ) in

    let add_constrain_next_rules_fwd p f filter f' = 
        let pf = mu_to_pdg_control p f in
        CharacterSet.foreach alphabet (fun a ->
            if filter (p, a) then
                NextSet.foreach (pds#get_next p a) (fun (p', w) ->
                    let p'f' = mu_to_pdg_control p' f' in
                    let rule = make_rule pf a p'f' w in
                    g_pds#add_rule rule
                )
            else ()
        ) in

    let add_next_rules p f f' = 
        add_constrain_next_rules_fwd p f (fun _ -> true) f' in

    let add_constrain_next_rules_bkwd p f filter f' = (
        let pf = mu_to_pdg_control p f in
        let eword = make_word [] in
        pds#pre_push_iter_p (fun b c hs ->
            HeadSet.iter (fun (p', a) ->
                let p'f' = mu_to_pdg_control_back c p' a f' in
                let rule = make_rule pf b p'f' eword in
                g_pds#add_rule rule;
                (* it's inefficient to do this here, but cleaner.  
                 * Fix it if needed 
                 *)
                let p''f' = mu_to_pdg_control p' f' in
                let rule' = make_rule p'f' c p''f' (make_word [a]) in
                g_pds#add_rule rule'
            ) hs
        ) p filter;
        pds#pre_rew_iter_p (fun b hs ->
            HeadSet.iter (fun (p', a) ->
                let p'f' = mu_to_pdg_control p' f' in
                let rule = make_rule pf b p'f' (make_word [a]) in
                g_pds#add_rule rule
            ) hs
        ) p filter;
        HeadSet.iter (fun (p', a) ->
            CharacterSet.iter (fun b ->
                let p'f' = mu_to_pdg_control p' f' in
                let rule = make_rule pf b p'f' (make_word [a; b]) in
                g_pds#add_rule rule
            ) alphabet 
        ) (pds#pre_pop_p p filter)
    ) in

    let add_next_rules_bkwd p f f' =
        add_constrain_next_rules_bkwd p f (fun _ -> true) f' in

    let add_constrain_next_rules p f op ids f' =
        let hs = get_sat_heads ids in
        let is_in h = HeadSet.mem h hs in
        let not_in h = not (HeadSet.mem h hs) in
        match op with
          ConstrainBox -> add_constrain_next_rules_fwd p f is_in f'
        | ConstrainDiamond -> add_constrain_next_rules_fwd p f is_in f'
        | ConstrainBackBox -> add_constrain_next_rules_bkwd p f is_in f'
        | ConstrainBackDiamond -> add_constrain_next_rules_bkwd p f is_in f'
        | NegConstrainBox -> 
              add_constrain_next_rules_fwd p f not_in f'
        | NegConstrainDiamond ->
              add_constrain_next_rules_fwd p f not_in f'
        | NegConstrainBackBox ->
              add_constrain_next_rules_bkwd p f not_in f'
        | NegConstrainBackDiamond ->
              add_constrain_next_rules_bkwd p f not_in f' in

    let add_prop_rules p id =
        let pf = mu_to_pdg_control p (Prop(id)) in
        let heads = get_sat_heads [id] in
        CharacterSet.foreach alphabet (fun a ->
            let resp = if HeadSet.mem (p, a) heads then psat else punsat in
            let rule = make_rule pf a resp (make_word [a]) in
            g_pds#add_rule rule
        ) in

    let add_neg_prop_rules p id =
        let pf = mu_to_pdg_control p (NegProp(id)) in
        let heads = get_sat_heads [id] in
        CharacterSet.foreach alphabet (fun a ->
            let resp = if HeadSet.mem (p, a) heads then punsat else psat in
            let rule = make_rule pf a resp (make_word [a]) in
            g_pds#add_rule rule
        ) in

    ControlStateSet.foreach controls (fun p ->
        MuSet.foreach subs (fun f ->
            let pf = mu_to_pdg_control p f in
            match f with
              True -> 
                failwith ("add_rules found a 'True' in normed fmla!")
            | False -> 
                failwith ("add_rules found a 'False' in normed fmla!")
            | Prop(id) -> add_prop_rules p id
            | NegProp(id) -> add_neg_prop_rules p id
            | Var(_) -> ()
            | Bin(f1, And, f2) -> (let pf1 = mu_to_pdg_control p f1 in
                                   let pf2 = mu_to_pdg_control p f2 in
                                   add_state_change_rules pf pf1;
                                   add_state_change_rules pf pf2)
            | Bin(f1, Or, f2) -> (let pf1 = mu_to_pdg_control p f1 in
                                  let pf2 = mu_to_pdg_control p f2 in
                                  add_state_change_rules pf pf1;
                                  add_state_change_rules pf pf2)
            | Bin(_, Implies, _) -> 
                failwith ("add_rules found an implication in normed fmla!")
            | Un(Box,f1) -> add_next_rules p f f1
            | Un(Diamond,f1) -> add_next_rules p f f1
            | Un(BackBox,f1) -> add_next_rules_bkwd p f f1
            | Un(BackDiamond,f1) -> add_next_rules_bkwd p f f1
            | Un(Not,_) -> 
                failwith ("add_rules found a negation in normed fmla ")
            | ConstrainUn(op,ids,f1) -> add_constrain_next_rules p f op ids f1
            | Mu(id, f1) -> (let pz = mu_to_pdg_control p (Var(id)) in
                             let pf1 = mu_to_pdg_control p f1 in
                             add_state_change_rules pf pz;
                             add_state_change_rules pz pf1)
            | Nu(id, f1) -> (let pz = mu_to_pdg_control p (Var(id)) in
                             let pf1 = mu_to_pdg_control p f1 in
                             add_state_change_rules pf pz;
                             add_state_change_rules pz pf1)
        )
    );;


let pdmu_to_pdg pdmu = 
    let pds : Pds.pds = pdmu#get_pds in
    let fmla = pdmu#get_fmla in
    let g_pds = new pds in
    let norm_f = normalise_formula fmla in
    let subs = get_subformulas norm_f in
    let controls = pds#get_control_states in
    let psat = make_control_state "psat" in
    let punsat = make_control_state "punsat" in

    let fmla_parity = 
        (* choose 1 if first fixed point is least, 0 if greatest *)
        let rec first_parity f =
            match f with
              True | False | Prop(_) | NegProp(_) | Var(_) -> 0
            | Bin(f1, _, f2) -> min (first_parity f1) (first_parity f2)
            | Un(_, f1) -> first_parity f1
            | ConstrainUn(_, _, f1) -> first_parity f1
            | Mu(_, _) -> 1
            | Nu(_, _) -> 0 in
        let join_maps m1 m2 = 
            FmlaParityMap.fold (fun v d m -> FmlaParityMap.add v d m) m1 m2 in
        let next_odd n =
            if n mod 2 = 0 then n + 1 else n in
        let next_even n =
            if n mod 2 = 1 then n + 1 else n in
        let rec diver f depth = 
            match f with
              True | False | Prop(_) | NegProp(_) -> FmlaParityMap.add f depth (FmlaParityMap.empty)
            | Var(id) -> FmlaParityMap.empty
            | Bin(f1, _, f2) -> let m1 = diver f1 depth in
                                let m2 = diver f2 depth in
                                FmlaParityMap.add f depth (join_maps m1 m2)
            | Un(_, f1) -> FmlaParityMap.add f depth (diver f1 depth)
            | ConstrainUn(_, _, f1) -> FmlaParityMap.add f depth (diver f1 depth)
            | Mu(id, f1) -> let depth' = next_odd depth in
                            let m = FmlaParityMap.add (Var(id)) depth' (diver f1 depth') in
                            FmlaParityMap.add f depth' m
            | Nu(id, f1) -> let depth' = next_even depth in
                            let m = FmlaParityMap.add (Var(id)) depth' (diver f1 depth') in
                            FmlaParityMap.add f depth' m in
        diver norm_f (first_parity norm_f) in

    let (min_colour, max_colour) = 
        FmlaParityMap.fold (fun _ d (dmin, dmax) -> (min d dmin, max d dmax)) 
                           fmla_parity 
                           (max_int, min_int) in

    let add_back_props f ps =
        let do_pre _ _ b hs ps = 
            let do_head (p, a) ps =
                ((mu_to_pdg_control_back b p a f), E, max_colour)::ps in
            HeadSet.fold do_head hs ps in
        pds#pre_push_fold do_pre ps in

    let props = 
        ControlStateSet.fold (fun p ps -> 
            MuSet.fold (fun f ps ->
                let pf = mu_to_pdg_control p f in
                let c = FmlaParityMap.find f fmla_parity in
                match f with
                  True -> failwith ("pds_mucalc#to_pdg found a 'True' in normed fmla " ^  
                                       (mu_formula_to_string fmla))
                | False -> failwith ("pds_mucalc#to_pdg found a 'False' in normed fmla " ^  
                                       (mu_formula_to_string fmla))
                | Prop(id) -> (pf, E, min_colour)::ps (* Just moves to appropriate sink state *)
                | NegProp(id) -> (pf, E, min_colour)::ps
                | Var(_) -> (pf, E, c)::ps
                | Bin(f1, And, f2) -> (pf, A, c)::ps
                | Bin(f1, Or, f2) -> (pf, E, c)::ps
                | Bin(_, Implies, _) -> failwith ("pds_mucalc#to_pdg found an implication in normed fmla " ^  
                                                  (mu_formula_to_string fmla))
                | Un(Box, _) -> (pf, A, c)::ps
                | Un(Diamond, f1) -> (pf, E, c)::ps
                | Un(BackBox, f1) -> (pf, A, c)::(add_back_props f1 ps)
                | Un(BackDiamond, f1) -> (pf, E, c)::(add_back_props f1 ps)
                | Un(Not,_) -> failwith ("pds_mucalc#to_pdg found a negation in normed fmla " ^  
                                       (mu_formula_to_string fmla))
                | ConstrainUn(ConstrainBox, _, _) -> (pf, A, c)::ps
                | ConstrainUn(NegConstrainBox, _, _) -> (pf, A, c)::ps
                | ConstrainUn(ConstrainBackBox, _, f1) -> (pf, A, c)::(add_back_props f1 ps)
                | ConstrainUn(NegConstrainBackBox, _, f1) -> (pf, A, c)::(add_back_props f1 ps)
                | ConstrainUn(ConstrainDiamond, _, _) -> (pf, E, c)::ps
                | ConstrainUn(NegConstrainDiamond, _, _) -> (pf, E, c)::ps
                | ConstrainUn(ConstrainBackDiamond, _, f1) -> (pf, E, c)::(add_back_props f1 ps)
                | ConstrainUn(NegConstrainBackDiamond, _, f1) -> (pf, E, c)::(add_back_props f1 ps)
                | Mu(id, _) -> (pf, E, c)::ps
                | Nu(id, _) -> (pf, E, c)::ps
            ) subs ps
        ) controls [] in

    let all_props = (psat, A, min_colour)::(punsat, E, min_colour)::props in
    add_rules pds subs (pdmu#get_sat_heads) psat punsat g_pds;

    let interesting_configs =
        let conv_control p = mu_to_pdg_control p norm_f in
        let conv_config (p, w) = (conv_control p, w) in
        List.map conv_config pdmu#get_interesting_configs in

    new parity_game g_pds (new property_map all_props) interesting_configs;;


let pdmu_to_pdrg pdmu =
    let pds : Pds.pds = pdmu#get_pds in
    let fmla = pdmu#get_fmla in
    let g_pds = new pds in
    let norm_f = normalise_formula fmla in
    let subs = get_subformulas norm_f in
    let controls = pds#get_control_states in
    let psat = make_control_state "psat" in
    let punsat = make_control_state "punsat" in

    let is_reach = not (MuSet.exists (fun f ->
        match f with
          Nu(_, _) -> true 
        | _ -> false
    ) subs) in

    if not is_reach then 
        failwith ("Converting non-reachability formula to reachability game!")
    else (
        let add_back_owners f ps =
            let do_pre _ _ b hs ps = 
                let do_head (p, a) ps =
                    ((mu_to_pdg_control_back b p a f), E)::ps in
                HeadSet.fold do_head hs ps in
            pds#pre_push_fold do_pre ps in

        let owners = 
            ControlStateSet.fold (fun p ps -> 
                MuSet.fold (fun f ps ->
                    let pf = mu_to_pdg_control p f in
                    match f with
                      True -> failwith ("pds_mucalc#to_pdg found a 'True' in normed fmla " ^  
                                           (mu_formula_to_string fmla))
                    | False -> failwith ("pds_mucalc#to_pdg found a 'False' in normed fmla " ^  
                                           (mu_formula_to_string fmla))
                    | Prop(id) -> (pf, E)::ps (* Just moves to appropriate sink state *)
                    | NegProp(id) -> (pf, E)::ps
                    | Var(_) -> (pf, E)::ps
                    | Bin(f1, And, f2) -> (pf, A)::ps
                    | Bin(f1, Or, f2) -> (pf, E)::ps
                    | Bin(_, Implies, _) -> failwith ("pds_mucalc#to_pdg found an implication in normed fmla " ^  
                                                      (mu_formula_to_string fmla))
                    | Un(Box, _) -> (pf, A)::ps
                    | Un(Diamond, f1) -> (pf, E)::ps
                    | Un(BackBox, f1) -> (pf, A)::(add_back_owners f1 ps)
                    | Un(BackDiamond, f1) -> (pf, E)::(add_back_owners f1 ps)
                    | Un(Not,_) -> failwith ("pds_mucalc#to_pdg found a negation in normed fmla " ^  
                                           (mu_formula_to_string fmla))
                    | ConstrainUn(ConstrainBox, _, _) -> (pf, A)::ps
                    | ConstrainUn(NegConstrainBox, _, _) -> (pf, A)::ps
                    | ConstrainUn(ConstrainBackBox, _, f1) -> 
                            (pf, A)::(add_back_owners f1 ps)
                    | ConstrainUn(NegConstrainBackBox, _, f1) -> 
                            (pf, A)::(add_back_owners f1 ps)
                    | ConstrainUn(ConstrainDiamond, _, _) -> (pf, E)::ps
                    | ConstrainUn(NegConstrainDiamond, _, _) -> (pf, E)::ps
                    | ConstrainUn(ConstrainBackDiamond, _, f1) -> 
                            (pf, E)::(add_back_owners f1 ps)
                    | ConstrainUn(NegConstrainBackDiamond, _, f1) -> 
                            (pf, E)::(add_back_owners f1 ps)
                    | Mu(id, _) -> (pf, E)::ps
                    | Nu(id, _) -> (pf, E)::ps
                ) subs ps
            ) controls [] in

        let all_owners = (psat, A)::(punsat, E)::owners in
        add_rules pds subs (pdmu#get_sat_heads) psat punsat g_pds;

        let target_heads = 
            CharacterSet.fold (fun a s -> 
                HeadSet.add (psat, a) s 
            ) pds#get_alphabet HeadSet.empty in

        let interesting_configs =
            let conv_control p = mu_to_pdg_control p norm_f in
            let conv_config (p, w) = (conv_control p, w) in
            List.map conv_config pdmu#get_interesting_configs in

        new reachability_game g_pds all_owners target_heads interesting_configs
    );;


(* returns (reachability game, config map)
 * where
 *    reachability game is game where Eloise tries to show Abelard could
 *    have won the parity game (i.e. if she wins, she would have lost
 *    parity)
 *
 *    config_map is map from interesting configs in parity game to
 *    equivalent config in reachability game
 *)
let pdg_to_pdmu pdg = 
    let pds = pdg#get_pds in
    let props = pdg#get_props in
    let min_c = props#get_min_colour in
    let max_c = props#get_max_colour in
    let controls = pds#get_control_states in
    let alphabet = pds#get_alphabet in

    let e_id = "E" in
    let ep = Prop(e_id) in

    let c_prop_id c = 
        string_of_int c in
    let make_c_prop c = 
        Prop(c_prop_id c) in

    let c_var_id c = 
        "Z" ^ (string_of_int c) in
    let make_c_var c = 
        Var(c_var_id c) in

    let rec colour_imps min max modal =
        let pc = make_c_prop max in
        let zc = make_c_var max in
        let imp = Bin(pc, Implies, Un(modal, zc)) in
        if min = max then
            imp
        else    
            Bin(imp, And, colour_imps min (max - 1) modal) in

    let f_e_imps = colour_imps min_c max_c Diamond in
    let f_a_imps = colour_imps min_c max_c Box in

    let f_e = Bin(ep, Implies, f_e_imps) in
    let f_a = Bin(Un(Not, ep), Implies, f_a_imps) in

    let phi_E = Bin(f_e, And, f_a) in

    let rec make_phi min max =
        if min > max then
            phi_E
        else if (min mod 2 = 0) then
            Nu(c_var_id min, make_phi (min + 1) max)
        else
            Mu(c_var_id min, make_phi (min + 1) max) in

    let phi = make_phi min_c max_c in

    let props_list = 
        ControlStateSet.fold (fun p ps ->
            let col_id = c_prop_id (props#get_colour p) in
            let is_E = (props#get_owner p = E) in
            CharacterSet.fold (fun a ps ->
                if is_E then
                    (p, a, [col_id; e_id])::ps
                else
                    (p, a, [col_id])::ps
            ) alphabet ps
        ) controls [] in
    let props = new proposition_map props_list in
    
    new pds_mucalc pds pdg#get_interesting_configs phi props;;

module OrderedConfig =
    struct
        type t = config
        let compare c1 c2 = 
            compare (config_to_string c1) (config_to_string c2) 
    end;;

(* (varname : string) -> (0 / 1) *)
module ConfigMap = Map.Make(OrderedConfig);;

let pdg_to_pdrg pdg =
    let pds = pdg#get_pds in
    let props = pdg#get_props in
    let min_c = props#get_min_colour in
    let max_c = props#get_max_colour in
    let controls = pds#get_control_states in
    let alphabet = pds#get_alphabet in
    let eword = make_word [] in

    let p_odd_win = make_control_state "_odd_win" in
    let old_sbot = make_character "_old_sbot" in

    (* Convert sbot to old_sbot -- we need a new one *)
    let conv_char a =
        if a = sbot then old_sbot else a in

    let colour_to_char c =
        Words.make_character ("_c" ^ (string_of_int c)) in

    (* staggering a push of three chars -- see translate push rule *)
    let char_intermediate a =
        let a_string = character_to_string a in
        Words.make_character ("_intermediate(" ^ a_string ^ ")") in

    (* A version of control state a that is check(p) -- i.e. opponent can
     * force a check of counter *)
    let check_control p =
        let p_string = control_state_to_string p in
        let check_p = "_check(" ^ p_string ^ ")" in
        make_control_state check_p in

    (* Create a state pop(p,c)
     * pop(p,c) means pop down to next char, ultimately arriving in state
     * p, but record min colour seen on the way (from the stack) *)
    let pop_to_p_state p c =
        let p_string = control_state_to_string p in
        let c_string = (string_of_int c) in
        let name = "_pop(" ^ p_string ^ "," ^ c_string ^ ")" in
        make_control_state name in

    (* A control state from which counter c is to be tested *)
    let check_counter_p c =
        let c_string = string_of_int c in
        let name = "_check_counter(" ^ c_string ^ ")" in
        make_control_state name in

    (* A state check(c, p, k) means the number of c increments we have
     * read on the stack is k when modded by p.*)
    let check_c_mod_p_is_k c p k = 
        let c_string = string_of_int c in
        let p_string = string_of_int p in
        let k_string = string_of_int k in
        let name = "_check(" ^ c_string ^ "," ^ p_string ^ "," ^ k_string ^ ")" in 
        make_control_state name in

    let new_pds = new pds in

    (* TRANSLATE PDS RULES *)
    (* After each move, go to a marked version of top of stack char from
     * which opponent can force a counter check *)

    (* go to a state where you pop down to next real char, keeping track
     * of min colour seen since push.  Counter check done at end (see
     * add pop to rules) *)
    let translate_pop_rule p a p' = 
        let col = props#get_colour p' in
        let p'_pop = pop_to_p_state p' col in
        let new_a = conv_char a in
        let r = make_rule p new_a p'_pop eword in
        new_pds#add_rule r in

    (* add colour of state to be reached below on stack *)
    let translate_rew_rule p a p' b = 
        let col = props#get_colour p' in
        let col_char = colour_to_char col in
        let check_p' = check_control p' in
        let new_a = conv_char a in
        let new_b = conv_char b in
        let rhs = make_word [new_b; col_char] in
        let r = make_rule p new_a check_p' rhs in
        new_pds#add_rule r in

    (* add colour of state to be seen underneath new top char *)
    let translate_push_rule p a p' b c =
        let col = props#get_colour p' in
        let col_char = colour_to_char col in
        let check_p' = check_control p' in
        let new_a = conv_char a in
        let new_b = conv_char b in
        let new_c = conv_char c in
        let middle_b = char_intermediate new_b in
        let middle_rhs = make_word [middle_b; new_c] in
        let r_middle = make_rule p new_a p' middle_rhs in
        new_pds#add_rule r_middle;
        let finish_rhs = make_word [new_b; col_char] in
        let r_finish = make_rule p' middle_b check_p' finish_rhs in
        new_pds#add_rule r_finish in
    
    pds#rules_iter (fun r ->
        let (p, a, p', w) = rule_tuple r in
        let w_list = word_to_list w in
        let w_len = List.length w_list in
        match w_len with
          0 -> translate_pop_rule p a p'
        | 1 -> translate_rew_rule p a p' (List.hd w_list)
        | 2 -> translate_push_rule p a p' (List.hd w_list) (List.hd (List.tl w_list))
        | _ -> failwith ("pdg_to_pdrg failed on rule " ^ 
                          (rule_to_string r) ^ 
                          " -- rule pushes too many characters (max = 2).")
    );

    (* ADD POP TO RULES *)
    (* Rules that pop the pushed counter values from the stack *)
    ControlStateSet.foreach controls (fun p -> 
        range_iter min_c max_c (fun c ->
            let pop_p = pop_to_p_state p c in
            (* When popping colours, keep track of the minimum *)
            range_iter min_c max_c (fun c' -> 
                let pop_p' = pop_to_p_state p (min c c') in
                let col_char = colour_to_char c' in
                let r = make_rule pop_p col_char pop_p' eword in
                new_pds#add_rule r
            );
            (* When a stack character is reached *)
            CharacterSet.foreach alphabet (fun a -> 
                let col_char = colour_to_char c in
                let check_p = check_control p in
                let new_a = conv_char a in
                let w = make_word [new_a; col_char] in
                let r = make_rule pop_p new_a check_p w in
                new_pds#add_rule r
            )
        )
    );

    (* ADD BOUNDS CHECK RULES *)
    let num_controls = ControlStateSet.cardinal controls in
    let num_alphabet = CharacterSet.cardinal alphabet in
    let num_colours = max_c - min_c + 1 in
    let exponential = 
        int_of_float (2. ** (float_of_int (num_controls * num_colours))) in
    let bound = 
        num_colours * num_controls * num_alphabet *  exponential in
    let prod_primes = primes_over_bound bound in
    
    (* check(a) can accept or challenge *)
    ControlStateSet.foreach controls (fun p ->
        CharacterSet.foreach alphabet (fun a ->
            let check_p = check_control p in
            let new_a = conv_char a in
            let w = make_word [new_a] in
            let r_accept = make_rule check_p new_a p w in
            new_pds#add_rule r_accept;

            (* challenge any counter that is odd *)
            range_iter min_c max_c (fun c ->
                if c mod 2 = 1 then (
                    let check_c = check_counter_p c in
                    let r_challenge = 
                        make_rule check_p new_a check_c eword in
                    new_pds#add_rule r_challenge
                )
            )
        )
    );

    range_iter min_c max_c (fun c ->
        if c mod 2 = 1 then (
            let check_p = check_counter_p c in
            (* Letters are always ignored in a challenge *)
            CharacterSet.foreach alphabet (fun a ->
                let new_a = conv_char a in
                let r = make_rule check_p new_a check_p eword in
                new_pds#add_rule r
            );
            (* When we pop an increment to c we can decide to go
             * into prime check mode for some prod_prime *)
            let col_char = colour_to_char c in
            List.iter (fun pr ->
                let check_c_pr_1 = check_c_mod_p_is_k c pr 1 in
                let r = make_rule check_p col_char check_c_pr_1 eword in
                new_pds#add_rule r
            ) prod_primes
        )
    );

    (* In prime check mode check(c, p, k) what can we do... *)
    range_iter min_c max_c (fun c ->
        if c mod 2 = 1 then (
            List.iter (fun p ->
                range_iter 0 (p-1) (fun k -> 
                    let pck = check_c_mod_p_is_k c p k in
                    (* Letters are always ignored in a challenge *)
                    CharacterSet.foreach alphabet (fun a ->
                        let new_a = conv_char a in
                        let r = make_rule pck new_a pck eword in
                        new_pds#add_rule r
                    );
                    (* Ignore anything bigger *)
                    range_iter (c+1) max_c (fun c' ->
                        let col_char = colour_to_char c' in
                        let r = make_rule pck col_char pck eword in
                        new_pds#add_rule r
                    );
                    (* go mod for anything equal *)
                    let col_char = colour_to_char c in
                    let k' = (k + 1) mod p in
                    let pck' = check_c_mod_p_is_k c p k' in
                    let r = make_rule pck col_char pck' eword in
                    new_pds#add_rule r;
                    (* win if anything smaller and divisible *)
                    range_iter min_c (c - 1) (fun c' ->
                        let col_char = colour_to_char c' in
                        if k = 0 then (
                            let r = make_rule pck col_char p_odd_win eword in
                            new_pds#add_rule r
                        )
                    );
                    (* win at new_sbot if divisible *)
                    if k = 0 then (
                        let rhs = make_word [sbot] in
                        let r = make_rule pck sbot p_odd_win rhs in
                        new_pds#add_rule r
                    )
                )
            ) prod_primes
        )
    );

    (* Add End Game Rules *)
    (* Add a rule that just pops the top_char if can win *)
    let add_end_game_rules top_char = (
        let r_win = make_rule p_odd_win top_char p_odd_win eword in
        new_pds#add_rule r_win;
    ) in
   
    CharacterSet.foreach alphabet (fun a -> 
        let new_a = conv_char a in
        add_end_game_rules new_a
    );
    range_iter min_c max_c (fun c -> 
        let col_char = colour_to_char c in
        add_end_game_rules col_char
    );

    (* Target new / genuine bottom of stack *)
    let target_heads = HeadSet.singleton (p_odd_win, sbot) in
    (* Translate interesting_configs: these almost just transfer across
     * as they represent configs with 0 counters, but need new bottom of
     * stack dealt with *)
    let config_map = ref ConfigMap.empty in
    let tran_conf (p, w) =
        let w'_list = List.map conv_char (word_to_list w) in
        let w'_list_sbot = List.append w'_list [sbot] in
        let w' = make_word w'_list_sbot in
        config_map := ConfigMap.add (p,w) (p, w') !config_map;
        (p, w') in
    let interesting_configs = 
        List.map tran_conf (pdg#get_interesting_configs) in 

    (* Finally, if Eloise would have gotten stuck in parity game, this
     * is a win for Abelard *)
    ControlStateSet.iter (fun p ->
        if (props#get_owner p) = E then (
            CharacterSet.iter (fun a ->
                if pds#get_next p a = NextSet.empty then (
                    let new_a = conv_char a in
                    let rhs = make_word [new_a] in
                    let r = make_rule p new_a p_odd_win rhs in
                    new_pds#add_rule r
                )
            ) alphabet
        )
    ) controls;

    (* Set owners so that Eloise is trying to prove Abelard would win
     * the parity game, so she is trying to reach the high counters and
     * players of the old game are switched *)
    let plain_owners = ControlStateSet.fold (fun p owns ->
        let o = props#get_owner p in
        (p, opp_owner o)::owns
    ) controls [] in
    let pop_owners = ControlStateSet.fold (fun p owns ->
        range_fold_left (fun owns c ->
            (pop_to_p_state p c, E)::owns
        ) owns min_c max_c
    ) controls [] in
    let check_owners = ControlStateSet.fold (fun p owns ->
        (check_control p, E)::owns
    ) controls [] in
    let check_c_owners = range_fold_left (fun owns c ->
        if (c mod 2 = 1) then 
            (check_counter_p c, A)::owns
        else 
            owns
    ) [] min_c max_c in
    let check_cpk_owners = range_fold_left (fun owns c ->
        if (c mod 2 = 1) then (
            List.fold_left (fun owns p ->
                range_fold_left (fun owns k ->
                    (check_c_mod_p_is_k c p k, E)::owns 
                ) owns 0 (p-1)
            ) owns prod_primes
        ) else (
            owns
        )
    ) [] min_c max_c in
    let end_game_owners = [(p_odd_win, E)] in
    let owners = List.concat [
        plain_owners;
        pop_owners;
        check_owners;
        check_c_owners;
        check_cpk_owners;
        end_game_owners
    ] in

    (* Phew *)
    let rg = 
        new reachability_game new_pds owners target_heads interesting_configs in
    (rg, !config_map);;

