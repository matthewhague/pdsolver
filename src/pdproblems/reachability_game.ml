
open Pds;;
open Words;;
open Game_defs;;

module OwnerMap = Map.Make(OrderedControlState);;

class pdrg_stats (pds_stats : pds_stats) =
    object
        method get_pds_stats = pds_stats

        method to_string =
            pds_stats#to_string

        method to_row_string =
            pds_stats#to_row_string
    end;;


class reachability_game (pds : pds)
                        (owners_list : (control_state * owner) list)
                        (target_heads : HeadSet.t) 
                        (interesting_configs : config list) = 
    object (self)
        val pds = pds
        val target_heads = target_heads
        val interesting_configs = interesting_configs

        val owners 
            = List.fold_left (fun m (p, o) -> OwnerMap.add p o m) 
                             OwnerMap.empty
                             owners_list

        method get_interesting_configs = interesting_configs
        method get_target_heads = target_heads
        method get_pds = pds

        method has_interesting_configs = 
            not (interesting_configs = [])

        method get_owner c = OwnerMap.find c owners

        method private configs_to_string =
            List.fold_right (fun (p, w) s -> 
                (control_state_to_string p) ^ " " ^
                (word_to_string w) ^ ";\n" ^ 
                s
            ) interesting_configs ""

        method private owners_to_string =
            OwnerMap.fold (fun p o s -> 
                (control_state_to_string p) ^ " " ^
                (owner_to_string o) ^ ";\n" ^
                s
            ) owners ""
 
        method private target_heads_to_string =
            HeadSet.fold (fun (p, a) s -> 
                (control_state_to_string p) ^ " " ^
                (character_to_string a) ^ ";\n" ^
                s
            ) target_heads ""
      
        method to_string = 
            ("Pds:\n\n" ^ pds#to_string ^ 
             "\n\nControl States:\n\n" ^
             self#owners_to_string ^
             "\n\nTarget Heads:\n\n" ^ 
             (self#target_heads_to_string) ^
             "\n\nInteresting Configs:\n\n" ^ 
             self#configs_to_string)

        method to_channel c = 
            output_string c "Pds:\n\n";
            pds#to_channel c;
            output_string c  "\n\nControl States:\n\n";
            output_string c (self#owners_to_string);
            output_string c "\n\nTarget Heads:\n\n";
            output_string c (self#target_heads_to_string);
            output_string c "\n\nInteresting Configs:\n\n";
            output_string c (self#configs_to_string);

        method equal (pdrg : reachability_game) =
            pds#equal (pdrg#get_pds) &&
            (target_heads = pdrg#get_target_heads) &&
            (interesting_configs = pdrg#get_interesting_configs)

        method get_stats = 
            new pdrg_stats (pds#get_stats)

    end;;
