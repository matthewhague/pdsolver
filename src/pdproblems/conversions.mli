
open Pds_mucalc;;
open Parity_game;;
open Reachability_game;;
open Mucalc;;
open Pds;;

module ConfigMap : Map.S with type key = config;;

val pdmu_to_pdg : pds_mucalc -> parity_game
val pdmu_to_pdrg : pds_mucalc -> reachability_game
val pdg_to_pdmu : parity_game -> pds_mucalc
(* convert parity game to negated safety game -- i.e. your opponent
 * tries to reach the target set *)
val pdg_to_pdrg : parity_game -> reachability_game * config ConfigMap.t

val mu_to_pdg_control : control_state -> mu_formula -> control_state 


