
open Pds;;
open Words;;
open Game_defs;;

class pdrg_stats :
    pds_stats ->
    object
        method get_pds_stats : pds_stats
        method to_string : string
        method to_row_string : string
    end;;

class reachability_game : 
    pds -> ((control_state * owner) list) -> HeadSet.t -> config list -> 
    object 
        method get_pds : pds
        method get_target_heads : HeadSet.t
        method get_interesting_configs : config list

        method has_interesting_configs : bool

        method get_owner : control_state -> owner

        method to_string : string

        method to_channel : out_channel -> unit

        method equal : reachability_game -> bool

        method get_stats : pdrg_stats
    end



