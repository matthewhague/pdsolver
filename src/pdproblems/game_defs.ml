
open Pds;;

type owner = A | E;;

let owner_to_string o = 
    match o with 
      A -> "Abelard"
    | E -> "Eloise";;

let opp_owner o =
    match o with
      A -> E
    | E -> A;;

module OrderedControlState =
    struct
        type t = control_state
        let compare = compare
    end;;


